

#Sophie Gresham - EEC Summer Project 2017 - 17.05.2017
#Choosing individuals/populations to use in the proxy source populations and test populations
#Plotting populations on a world map with admixture coefficients - to determine which populations are admixed and un-admixted

rm(list = ls())

#method 1:
#choose all individuals with a African/Non-African coefficient above a certain threshold?
setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE/results 10.05.2017 - 2-20/plots")

#from correlation matrix K=16 is chosen to be the optimum
#subset un-admixed individuals from the editted K16 dataset
#want around 50 individuals in each proxy population
#set the threshold African/NonAfrican IAC to be 0.98 for now
#test other thresholds later - 0.97 and 0.96 etc. 

#plot histogram - see the distribution of African and Non-African IAC
#to decide what the threshold should be 
data16 <- read.csv("260k_LD_PrunedIACedit16.csv")
cutdata <- cut(data16$African_IAC, 50, include.lowest=TRUE)
par(cex.axis=1, las=2)
plot(cutdata)

#subset individuals with African/Non-African IAC higher than 0.95%:
AfricaIACunadmixted <- subset(data16, data16$African_IAC >= 0.95)
NonAfricaIACunadmixted <- subset(data16, data16$Non_African_IAC >= 0.95)



#Admixture barplots:
#adapted from structure.plots function in structure_plots2 script
#use to determine which populations have individuals with low admixture and so should be used as proxy source populations

options(stringsAsFactors=F)
library(MASS)
library(RColorBrewer)
text.size=0.6
line.weight=0.8

#match the colours to the colours in the admixture map plot - used brewer palette
col1 <- brewer.pal(9,"Reds")
col2 <- brewer.pal(9,"Greens")
col3 <- brewer.pal(9,"Blues")
redcol <- col1[6]
greencol <- col2[6]
greencol2 <- col2[7]
bluecol <- col3[7]
colours <- c(redcol, greencol)

#plotting admixture
newdata <- data16[order(data16$region, data16$pop),] 
par(mar=c(5,5,1,1))
barplot(t(as.matrix(newdata[,21:22])), col=colours, space=0, border=NA, axes=T, axisnames=F, cex.axis=0.8,
        at=seq(0, 1, 0.1), ylab= "% Inferred African Ancestry")

#population axis label
factorpop <- as.factor(newdata$pop)
ppos <- c()
for (p in unique(factorpop)) {
  ppos <- c(ppos, mean(which(factorpop == p)))
}
axis(1, at=ppos, labels=unique(factorpop), tick=T, las=2, cex.axis=0.7)

#splitting up populations with lines
pmin <- c()
for (p in unique(factorpop)) {
  pmin <- c(pmin, min(which(factorpop == p)))
}
pmin <- pmin - 0.5
pmin <- pmin[pmin > 1]
abline(v=pmin, lwd=line.weight)

#abline(v=pmin, lwd=line.weight, cex.axis=0.8)


#admixture barplot just for population averages

#load average data
setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE")
avpops <- read.csv("orderedHOA_popdata_latlon.csv")
head(avpops)
avpops["Av_African_IAC"] <- 0
avpops["Av_Non_African_IAC"] <- 0
IACdata2 <- read.csv("~/Documents/SummerProject/Data/HOA/ADMIXTURE/results 10.05.2017 - 2-20/plots/260k_lD_PrunedIACedit16.csv")
for (k in 1:55){
  IACpop2 <- subset(IACdata2, IACdata2$pop == avpops$pop[k])
  avpops$Av_African_IAC[k] <- mean(IACpop2$African_IAC)
  avpops$Av_Non_African_IAC[k] <- mean(IACpop2$Non_African_IAC)
}

newdata2 <- avpops[order(avpops$region, avpops$pop),]
par(mar=c(5,5,1,1))
barplot(t(as.matrix(newdata2[,6:7])), col=colours, space=0, border=NA, axes=T, axisnames=F, cex.axis=0.8,
        at=seq(0, 1, 0.05), ylab= "Population Mean % Inferred African Ancestry")
factorpop2 <- as.factor(newdata2$pop)
ppos <- c()
for (p in unique(factorpop2)) {
  ppos <- c(ppos, mean(which(factorpop2 == p)))
}
axis(1, at=ppos-0.5, labels=unique(factorpop2), tick=T, las=2, cex.axis=0.7)
pmin <- c()
for (p in unique(factorpop2)) {
  pmin <- c(pmin, min(which(factorpop2 == p)))
}
pmin <- pmin
pmin <- pmin[pmin > 0]
abline(v=pmin, lwd=line.weight)

#axis(4, at=seq(0, 1, 0.05))

#chosen populations are:
    #African Source - Yoruba (YRH and YRI) with Sudanese, Gumuz, Anuak
    #Non-African Source - Druze (with other pop?)

###############################


#Method 2:
#can't have different individuals from the same population in the test pop and the source pop
#choose just a few proxy source populations?
#just use a couple of individuals from one un-admixed population and not use the rest?

setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE")

prepops <- read.csv("orderedHOA_popdata_latlon.csv")
head(prepops)
#subset out Gujarati and CEPH_Europeans - they are useful for genetic analyses but not for finding 
#source/proxy populations from the map - they are found in the USA but genetically they are Indian/European
pops <- prepops[!(prepops$pop %in% c("Gujarati", "CEPH_Europeans")),]

#Plotting populations on a map
library(rworldmap)
library(rworldxtra)
newmap <- getMap(resolution = "low")
par(mar=c(1,1,1,1))
plot(newmap, xlim = c(30, 50), ylim = c(-37, 71), asp = 1, border="darkgrey") #plot map outline
MEpops <- pops[(pops$region %in% c("Mid_East")),]
Afrpops <- pops[(pops$region %in% c("Africa")),]
Elsepops <- pops[!(pops$region %in% c("Africa", "Mid_East")),]
points(Afrpops$lon, Afrpops$lat, col = redcol, cex = 0.5, lwd=4) #plot population points
text(Afrpops$lon, Afrpops$lat, labels=Afrpops$pop, cex= 0.7, col=redcol, pos=3)#pop name labels
points(MEpops$lon, MEpops$lat, col = greencol2, cex = 0.5, lwd=4)
text(MEpops$lon, MEpops$lat, labels=MEpops$pop, cex= 0.7, col=greencol2, pos=3)
points(Elsepops$lon, Elsepops$lat, col = bluecol, cex = 0.5, lwd=4)
text(Elsepops$lon, Elsepops$lat, labels=Elsepops$pop, cex= 0.7, col= bluecol, pos=3)
dev.off()

#different map plot - using google maps:
#library(ggmap)
#map <- get_map(location = c(lon = mean(pops$lon), lat= mean(pops$lat)), zoom = 3, maptype= c("satellite"))
#mapPoints <- ggmap(map) +
#  +   geom_point(aes(x = pops$lon, y = pops$lat), data = pops, alpha = .5)
#http://www.milanor.net/blog/maps-in-r-plotting-data-points-on-a-map/ 

#Plotting African admixture coefficients on a world map - in order to identify
#using 16K dataset
preIACdata <- read.csv("~/Documents/SummerProject/Data/HOA/ADMIXTURE/results 10.05.2017 - 2-20/plots/260k_lD_PrunedIACedit16.csv")
#subset Gujarati and CEPH_Europeans out again
IACdata <- preIACdata[!(preIACdata$pop %in% c("Gujarati", "CEPH_Europeans")),]
IACdata["lon"] <- 0
IACdata["lat"] <- 0

for (i in 1:1363){
  for (k in 1:53){
    if (IACdata$pop[i] == pops$pop[k]){
      IACdata$lon[i] <- pops$lon[k]
      IACdata$lat[i] <- pops$lat[k]
    }
  }
}


colrp <- colorRampPalette(c("white","blue", "darkblue"))(20)
#clustermap <- 1
colpalette <- colrp
#mapadd <- T
# read spatial coordinates from the tess input file  
#coordinates <- IACdata[,23:24]
# read admixture (tess or clummp output)
#cluster <- IACdata[,21]
library(fields)
library(maps)
#fit <- Krig(coordinates, cluster, m= 1, theta = 10)
#surface(fit, col = colpalette, levels = c(-1), extrap = T, nx = 100, ny=100) #nx and ny set to 500 for high no. of pixels
#points(coordinates, cex = .4, col="red", pch = 19)
#text(pops$lon, pops$lat, labels=pops$pop, cex= 0.7, col="red", pos=3)
#if(mapadd) map(add=T, interior = F, lwd = 1)


#admixture averages from each population:
#is the shade of colour in the previous plot influenced by how many there are in a population?
#if so taking the average will fix problem
pops["Av_African_IAC"] <- 0
pops["Av_Non_African_IAC"] <- 0

for (k in 1:53){
  IACpop <- subset(IACdata, IACdata$pop == pops$pop[k])
  pops$Av_African_IAC[k] <- mean(IACpop$African_IAC)
  pops$Av_Non_African_IAC[k] <- mean(IACpop$Non_African_IAC)
}

#save editted pops info file
write.csv(pops, file=paste("orderedHOA_popdata_AvIAC.csv"))

#plot averages
coordinates2 <- pops[,4:5]
cluster2 <- pops[,6]
fit2 <- Krig(coordinates2, cluster2, m= 1, theta = 10)
admixture <- surface(fit2, col = colpalette, levels = c(-1), extrap = T)
points(coordinates2, cex = .4, col="black", pch = 19)
text(pops$lon, pops$lat, labels=pops$pop, cex= 0.7, col="black", pos=3)
map <- map(add=T, interior = F, lwd = 1, bg="white")
#plotting population averages - didnt change much

#getting rid of backgroud:
newad <- intersect(admixture, map)

nulldf <- data.frame("56", "Null", "Null", 1, 0.5, 0, 0)
names(nulldf) <- c("order", "region", "pop", "lon", "lat", "Av_African_IAC", "Av_Non_African_IAC")
newdf <- rbind(pops, nulldf)

coordinates2 <- pops[,4:5]
cluster2 <- pops[,6]
fit2 <- Krig(coordinates2, cluster2, m= 1, theta = 10)
surface(fit2, col = colpalette, levels = c(-1), extrap = T)
points(coordinates2, cex = .4, col="red", pch = 19)
#text(pops$lon, pops$lat, labels=pops$pop, cex= 0.7, col="red", pos=3)
if(mapadd) map(add=T, interior = F, lwd = 1)



#individual components:
cluster3 <- IACdata[,18]
fit <- Krig(coordinates, cluster3, m= 1, theta = 10)
surface(fit, col = colpalette, levels = c(-1), extrap = T)
points(coordinates, cex = .4, col="red", pch = 19)
#text(pops$lon, pops$lat, labels=pops$pop, cex= 0.7, col="red", pos=3)
if(mapadd) map(add=T, interior = F, lwd = 1)

#1 - Non-African
#2 - Non-African - European
#3 - Non-African
#4 - African - Pygmy 
#5 - Non-African - South Asian?
#6 - African - Ethiopic
#7 - African - Khoesan
#8 - Non-African - East Asian?
#9 - Non-African - Eurasian
#10 - Non-African
#11 - Non-African
#12 - Non-African - Ethio-Somali
#13 - Non-African
#14 - Non-African
#15 - African - Ethopic?
#16 - African - Niger-Congo


##############
#upload world map shape file
library(raster)
library(rgdal)
#setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE/results 10.05.2017 - 2-20/gadm28.shp")
#world <- shapefile("gadm28.shp")


#alternate plot 
#uses script POPsutilities
source('~/Documents/SummerProject/Data/HOA/ADMIXTURE/scripts/POPSutilities.R')
plot(pops[,4:5], pch = 19,  xlab="Longitude", ylab = "Latitude", cex=0.5)
map(add = T, interior = F, col = "grey80")

#grid=createGrid(min(pops[,"lon"]),max(pops[,"lon"]),
#                min(pops[,"lat"]),max(pops[,"lat"]),200,100)
#constraints=createGrid(min(pops[,"lon"]),max(pops[,"lon"]),
#             min(pops[,"lat"]),max(pops[,"lat"]), 100, 100)

setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE/AdmixtureMapPlot")
asc.raster = "testregion.txt" 
grid=createGridFromAsciiRaster(asc.raster)

constraints=getConstraintsFromAsciiRaster(asc.raster,cell_value_min=0)
#legend=displayLegend(K=2,colorGradientsList=lColorGradients) 

#define the colours for each population label
#colours <- c("black", "black", "white", "white", "white", "white", 
#             "black", "white", "white", "white", "white", "white", 
#             "white", "white", "white", "white", "white", "white",
#             "white", "white", "black", "white", "white", "white", 
#             "white", "white", "white", "white", "black", "white",
#             "white", "black", "white", "white", "white", "white",
#             "white", "white", "white", "white", "white", "white",
#             "white", "white", "white", "white", "white", "white",
#             "black", "white", "black", "white", "black")

par(mar=c(5,5,2,2))
maps(matrix = IACdata[,21:22], IACdata[,23:24], grid, constraints, method = "max", main = "Ancestry coefficients",
     xlab = "Longitude", ylab = "Latitude", cex=0.5)
text(pops$lon, pops$lat, labels=pops$pop, cex= 0.7, col=colours, pos=3)
#map(add = T, interior = F)
#plot(asc.raster)

#z-score delta values for each SNP - using Data2 dataframe from IntroCalc.
#ERN1 - rs9911085
#CSMD2 - rs923881
#ARHGEF4 - rs4530408

ERN1 <- Data2[(Data2$RS %in% "rs3742578"),]
ERN1$AD_LOC_ME <- (1 - ERN1$AD_LOC_AFR)
#ERN1$Z_DELTA_HIGH <- (5 - ERN1$Z_DELTA_AD)
#ERN1$Z_DELTA_LOW <- (5 + ERN1$Z_DELTA_AD)
ERN12 <- subset(ERN1, select=c("POP","AD_LOC_AFR","AD_LOC_ME"))
colnames(ERN12)[1] <- "pop"
ERN13 <- merge(IACdata, ERN12, by="pop", all.x=FALSE)
pops2 <- pops[(pops$region %in% c("Africa", "Mid_East" )),]
pops3 <- pops2[!(pops2$pop %in% c("Druze", "iran", "Yoruba_YRI", "Anuak" )),]

plot(pops3[,4:5], pch = 19,  xlab="Longitude", ylab = "Latitude", cex=0.5)
map(add = T, interior = F, col = "grey80")
par(mar=c(5,5,2,2))
maps(matrix = ERN13[,25:26], ERN13[,23:24], grid, constraints, method = "max", main = "",
     xlab = "Longitude", ylab = "Latitude", cex=0.5)
text(pops3$lon, pops3$lat, labels=pops3$pop, cex= 0.7, col="grey", pos=3)

