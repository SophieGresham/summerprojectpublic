#!/usr/bin/perl

use strict;
use warnings;

####################################################
#                 within.pl                        #
#  Creates a txtp file with the FIDs, IIDs and pop  #
#          names from given populations            #
####################################################

my $fam = $ARGV[0]; #fam file
my $pop = $ARGV[1]; #popfile
my $select = $ARGV[2]; #txt file with list of selected populations
my $out = $ARGV[3]; #output txt file with FIDs, IIDs and Pop name in each column 


unless ($fam and $pop and $select and $out) {
	die "\n\nERROR: Not enough arguments.\n\n";
}


unless (-f $fam) {
	die "\n\nERROR: Cannot find $fam. Check file path.\n\n";
}

unless (-f $pop) {
	die "\n\nERROR: Cannot find $pop. Check file path.\n\n";
}

unless (-f $select) {
	die "\n\nERROR: Cannot find $select. Check file path.\n\n";
}


open SELECT, "$select";

my %select; #  population => 0   ....just need key to exist

while (<SELECT>) {
	my $line = $_;
	chomp $line;
	my @l = split /\s/, $line;
	
	foreach my $i (@l) {
		unless (exists $select{$i}) {
			$select{$i} = 0;
		}
		
	}
}


close SELECT;



open POP, "$pop";

my %id; # ID => population 

while (<POP>) {
	my $line = $_;
	chomp $line;
	my @l = split /\s/, $line;
	my $id = $l[0];
	my $p = $l[3];
	if (exists $select{$p}) {
		unless (exists $id{$id}) {
			$id{$id} = $p;
		}
	}
}

close POP;


open FAM, "$fam";

#my @file = <FAM>;

my %fid; # IID => FID

while (<FAM>) {
	my $line = $_;
	chomp $line;
	my @l = split /\s/, $line;
	my $fid = $l[0];
	my $iid = $l[1];
	if (exists $id{$iid}) {
		unless (exists $fid{$iid}) {
			$fid{$iid} = $fid;
		}
	 }
}

close FAM;


open OUT, ">$out";

foreach my $i (keys %fid) {
	print OUT "$fid{$i}\t$i\t$id{$i}\n";
}

close OUT;




exit;