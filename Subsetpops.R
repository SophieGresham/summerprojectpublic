

#Sophie Gresham - EEC Summer Project 2017 - 30.05.2017
#Creating proxy source and test populations

#chosen populations are:
#African Source - Yoruba (YRH and YRI) with Sudanese, Gumuz, Anuak
#Non-African Source - Druze (with other pop?)

rm(list = ls())
setwd("~/Documents/SummerProject/Data/HOA/ADMIXTURE/results 10.05.2017 - 2-20/plots")
data16 <- read.csv("260k_LD_PrunedIACedit16.csv")

#afr <- subset(data16, data16$African_IAC >= 0.94)

#African source populations
#subset the dataset with the populations to be used as African proxy source populations

#all chosen source populations
AfrtotSP <- data16[(data16$pop %in% c("Yoruba_YRH", "Yoruba_YRI", "Sudanese", "Gumuz", "Anuak")),]
Afrsource <- subset(AfrtotSP, AfrtotSP$African_IAC >= 0.94) #60 individuals

#Yoruba_YRH
YorYRHSP <- data16[(data16$pop %in% c("Yoruba_YRH")),]
YorYRHsource <- subset(YorYRHSP, YorYRHSP$African_IAC >= 0.94) #6 individuals

#Yoruba_YRI
YorYRISP <- data16[(data16$pop %in% c("Yoruba_YRI")),]
YorYRIsource <- subset(YorYRISP, YorYRISP$African_IAC >= 0.94) #19 individuals

#Sudanese
SudSP <- data16[(data16$pop %in% c("Sudanese")),]
Sudsource <- subset(SudSP, SudSP$African_IAC >= 0.94) #11 individuals

#Gumuz
GumSP <- data16[(data16$pop %in% c("Gumuz")),]
Gumsource <- subset(GumSP, GumSP$African_IAC >= 0.94) #9 individuals
                                                      #extra Gum individual - GUMUZ20IBD - why? not supposed to be there
#Anuak
AnuSP <- data16[(data16$pop %in% c("Anuak")),]
Anusource <- subset(AnuSP, AnuSP$African_IAC >= 0.94) #15 individuals

#export ID's for source individuals
setwd("~/Documents/SummerProject/Data/HOA/bedfiles/SourceID")
write.table(Afrsource$iid, "AfrsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(YorYRHsource$iid, "YorYRHsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(YorYRIsource$iid, "YorYRIsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(Sudsource$iid, "SudsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(Gumsource$iid, "GumsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(Anusource$iid, "AnusourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

#Non-African source populations

#Druze
NonAfrsourcepops <- data16[(data16$pop %in% c("Druze", "iran")),]
NonAfrsource <- subset(NonAfrsourcepops, NonAfrsourcepops$Non_African_IAC >=0.94) #22

#Iran
Iransource <- data16[(data16$pop %in% c("iran")),]
IranSub <- subset(Iransource, Iransource$Non_African_IAC >= 0.94) #12

#Turkey
Turkeysource <- data16[(data16$pop %in% c("turkey")),]
TurkeySub <- subset(Turkeysource, Turkeysource$Non_African_IAC >= 0.94) #12


#export ID's for source individuals
write.table(NonAfrsource$iid, "../bedfiles/SourcePops/NonAfrsourceID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

#All source populations
#all chosen source populations
SourceTot <- data16[(data16$pop %in% c("Yoruba_YRI","Anuak", "Druze", "iran")),]
AfNonAfSourceTot <- subset(SourceTot, SourceTot$African_IAC >= 0.94) #60 individuals
Tot <- rbind(AfNonAfSourceTot, NonAfrsource)
write.table(Tot$iid, "TotAfrNonAfrID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

#All IDs - all test IDs and selected source IDs
testpops <- data16[(data16$pop %in% c("Afar", "Amhara", "Anuak", "Blacksmith", "Cultivator",
                                      "Biaka_Pygmy", "ESomali", "Gumuz", "Herero", "Juhoansi", "Karretjie", "Khomani",
                                      "Khwe", "Luhya", "Maasai", "Mandenka", "Mbuti_Pygmy", "Nama", "Oromo", "San",
                                      "Somali", "SABantu", "Sudanese", "Tygray", "Wolayta","Yoruba_YRH",
                                      "Bedouin", "egypt2", "lebanon", "Mozabite", "Palestinian", "Saudi",
                                      "syria", "turkey", "Yemen")),]
TotIDs <- rbind(testpops, Tot)
write.table(TotIDs$iid, "TotIDs.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)


#######

#prelimsubset
#test whether changing the source populations makes much of a difference on the fst results

Afr <- data16[(data16$pop %in% c("Yoruba_YRI","Anuak", "Yoruba_YRH", "Sudanese", "Gumuz")),]
AfrTot <- subset(Afr, Afr$African_IAC >= 0.94)
Non_Afr <- data16[(data16$pop %in% c("Druze", "iran", "turkey", "syria")),]
Non_AfrTot <- subset(Non_Afr, Non_Afr$Non_African_IAC >= 0.94)
PrelimTot <- rbind(AfrTot, Non_AfrTot)
#write.table(PrelimTot$iid, "PrelimID.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

#All IDs - all test IDs and selected source IDs
testpops2 <- data16[(data16$pop %in% c("Afar", "Amhara", "Blacksmith", "Cultivator",
                                      "Biaka_Pygmy", "ESomali", "Herero", "Juhoansi", "Karretjie", "Khomani",
                                      "Khwe", "Luhya", "Maasai", "Mandenka", "Mbuti_Pygmy", "Nama", "Oromo", "San",
                                      "Somali", "SABantu", "Tygray", "Wolayta",
                                      "Bedouin", "egypt2", "lebanon", "Mozabite", "Palestinian", "Saudi",
                                      "Yemen")),]
TotIDs2 <- rbind(testpops2, PrelimTot)
write.table(TotIDs2$iid, "TotIDsPrelim.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

