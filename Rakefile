#!/usr/local/bin/ruby

############################################################
#          Rakefile for HOA Data 05-2017                   #
############################################################

SCRIPTS = "scripts"
RESULTS = "results"

POP = "../bedfiles/HOA_combinedSGedit1.popfile"
#POP = "../Example_Data/UK_1000G.poptable"

PLINKIN = "../bedfiles/260k"
#PLINKIN = "../Example_Data/UK_1000G_b37"
BED = "#{PLINKIN}.bed"
FAM = "#{PLINKIN}.fam"

PLOTORDER = "orderedHOA_popdata.csv"
#PLOTORDER = "ordered_popdata.csv"

PREFIX = "260k"
#PREFIX = "UK_1000G"


CHR = "a" #include all autosomes

LD = 0.1
#LD = 0.25       # amount of LD pruning
THIN = 0.1      # proportion of SNPs to maintain 


CV = "#{RESULTS}/Cross_Validation"
CVMIN = 1
CVMAX = 20
#K = 2 #value of K chosen after evaluating cross validation results
#set min and max to 12 for main data
#also test higher and lower values for the 260K dataset - optimum k may be different from the 16K dataset
		#test from 2 to 20


#ADMIXTURE Plots Edit - from Rakefile2 script

#set parameters for admixture plots
TEXTSIZE = 0.3                                      # cex population label text size for plot
LINEWEIGHT = 0.4                                         # abline weight for line dividing populations in the plot                                       


# directory tasks

directory RESULTS


# processing tasks


PRUNE = "#{RESULTS}/#{PREFIX}_LD_Pruned"
BEDPRUNE = "#{PRUNE}.bed"

file BEDPRUNE => [RESULTS] do
	sh "./#{SCRIPTS}/Prune.pl #{PLINKIN} #{PRUNE} #{LD} #{THIN}"
	sh "mv plink.prune.in #{RESULTS}/#{PREFIX}.prune.in"
	sh "mv plink.prune.out #{RESULTS}/#{PREFIX}.prune.out"
	sh "mv plink.log #{RESULTS}/#{PREFIX}.log"
	sh "rm plink.nosex"
end







task :cv => [RESULTS] do
	sh "for k in {#{CVMIN}..#{CVMAX}}; \ do admixture --cv #{BEDPRUNE} $k | tee log${k}.out; done"
end

PRECLEAN = "#{PREFIX}_LD_Pruned"
QFILE = "#{PRECLEAN}.#{CVMAX}.Q"
PFILE = "#{PRECLEAN}.#{CVMAX}.P"

#directory RESULTS/CV => [RESULTS]

directory CV



task :cv_clean => [CV, QFILE, PFILE] do
	sh "grep -h CV log*.out > #{CV}/#{PRECLEAN}.CVerror"
	sh "for k in {#{CVMIN}..#{CVMAX}}; do mv #{PRECLEAN}.${k}.Q #{CV}; done"
	sh "for k in {#{CVMIN}..#{CVMAX}}; do mv #{PRECLEAN}.${k}.P #{CV}; done"
	sh "for k in {#{CVMIN}..#{CVMAX}}; do mv log${k}.out #{CV}; done"
end 

CVLOG = "#{CV}/log#{CVMAX}.out"

CVERROR = "#{CV}/CV_error"

directory CVERROR

task :cv_error => [CVLOG, CVERROR] do
	sh "grep -h CV #{CV}/log*.out >CVerror.out"
	sh "sed s/[\\(\\):kK=]//g CVerror.out >Rtable"
	sh "awk '{print $3,$4}' Rtable >#{CVERROR}/CVerror.Rtable"
	sh "rm CVerror.out"
	sh "rm Rtable"
	sh "./#{SCRIPTS}/errorPlot.R #{CVERROR}/CVerror.Rtable CVerror.plot.pdf"
	sh "mv CVerror.plot.pdf #{CVERROR}/"
end



PLOT = "#{RESULTS}/plots"

directory PLOT


task :plot => [PLOT] do
	sh "for k in {#{CVMIN}..#{CVMAX}}; do ./#{SCRIPTS}/admixture2R.pl #{CV}/#{PRECLEAN}.${k}.Q #{FAM} #{POP} #{PLOT}/#{PRECLEAN}.${k}.Rtable; done"
	sh "for k in {#{CVMIN}..#{CVMAX}}; do ./#{SCRIPTS}/plotcsv.pl #{PLOT}/#{PRECLEAN}.${k}.Rtable #{PLOT}/#{PRECLEAN}.${k}.csv; done"
	sh "for k in {#{CVMIN}..#{CVMAX}}; do ./#{SCRIPTS}/structure_plots2.R #{PLOT}/#{PRECLEAN}.${k}.csv #{PLOTORDER} #{PLOT}/#{PREFIX}_K${k}.pdf #{LINEWEIGHT} #{TEXTSIZE}; done"
end











#cleaning task

task :clobber => [RESULTS] do
	sh "rm -r #{RESULTS}"
end


task :default => [BEDPRUNE, :cv, :cv_clean, :cv_error, :plot]


#for k in {2..3}; do ./scripts/admixture2R.pl results/Cross_Validation/LD_pruned_reduced_genome.${k}.Q /Users/JAHodgson/yem370Ksnps/analysis/14a_Reduced_PCA_1-1-2012/results/LD_pruned_reduced_genome.fam Pop_Info_Coordinates.table LD_pruned_genome.${k}.Rtable; done

