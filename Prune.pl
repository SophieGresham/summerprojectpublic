#!/usr/bin/perl

use strict;
use warnings;

##################################################
# This prunes SNPs according to LD using Plink   #
##################################################

my $in = $ARGV[0]; #input plink file set prefix to be pruned
my $out = $ARGV[1]; #output file name
my $r2 = $ARGV[2]; # R square cutoff for pruning. Smaller numbers get rid of more SNPs (e.g. weaker linked).
my $percent;

if ($ARGV[3]) {
	$percent = $ARGV[3];
} else {
	$percent = 0;
}

unless (-f "$in.bed") { #check input file
	die "\n\nYou must specify an input file. Check file path.\n\n";
}

##################################################
#      Call Plink to perform LD-pruning          #
##################################################


my $file = "--bfile $in"; #input file prefix
my $missing = "--missing-genotype N"; #missing genotype indicator
my $LDprune = "--indep-pairwise 50 5 $r2"; #command for LD based pruning
my $extract = "--extract plink.prune.in"; #command for extracting SNPs
my $thin = "--thin $percent"; #command for percentage of SNPs to randomly retain
my $sex = "--allow-no-sex"; #no sex allowed!!!!
my $missing_SNP = "--geno 0.05";
my $missing_ind = "--mind 0.05";

system "plink $file $missing $sex $missing_SNP $missing_ind $LDprune"; #call plink to calculate SNPs to prune

if ($percent < 1 and $percent > 0) {
	system "plink $file $missing $sex $extract $thin --make-bed --out $out"; #call plink to extract pruned SNPs and randomly thin to specified percent
} else {
	system "plink $file $missing $sex $extract --make-bed --out $out";
}





exit;