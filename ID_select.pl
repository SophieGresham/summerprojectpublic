#!/usr/bin/perl

use strict;
use warnings;

####################################################
#                 ICD_select.pl                    #
#     Calls PLINK to select only given IDs         #
#       and subset bfiles given those IDs          #
####################################################

my $infile = $ARGV[0]; #txt file with list of IDs to keep
my $fam = $ARGV[1]; #fam file to be subsetted
my $outfile1 = $ARGV[2]; #output subsetted fam file
my $in = $ARGV[3]; #full data set in PLINK binary format
my $outfile2 = $ARGV[4]; #output subsetted bfiles


#################################################
#     Call PLINK to subset fam file             #
#################################################

open IN, "$infile";

my @ID = <IN>;

foreach my $i (@ID) {
	chomp $i;
	system "grep \'$i\' $fam >> $outfile1"; 
}

#################################################
#     Call PLINK to subset bfiles               #
#################################################

system "plink --bfile $in --missing-genotype N --keep $outfile1 --make-bed --out $outfile2" ;


exit;
