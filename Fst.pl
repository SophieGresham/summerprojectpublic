#!/usr/bin/perl

use strict;
use warnings;

####################################################
#                 within.pl                        #
#  Creates a txt file with the FIDs, IIDs and pop  #
#          names from given populations and        #
#  then runs Fst analysis on these two given pops  #
####################################################


my $fam = $ARGV[0]; #fam file
my $pop = $ARGV[1]; #popfile
my $select = $ARGV[2]; #txt file with list of selected population pairs
my $out1 = $ARGV[3]; #output txt file with FIDs, IIDs and Pop name in each column 
my $IDselect = $ARGV[4]; #ID list of individuals to keep - list of all test individuals and selected source individuals
my $out2 = $ARGV[5]; #subsetted output file with only selected individuals - using ID list
my $bfile = $ARGV[6]; #bfiles - 260k


unless ($fam and $pop and $select and $out1 and $out2) {
	die "\n\nERROR: Not enough arguments.\n\n";
}


unless (-f $fam) {
	die "\n\nERROR: Cannot find $fam. Check file path.\n\n";
}

unless (-f $pop) {
	die "\n\nERROR: Cannot find $pop. Check file path.\n\n";
}

unless (-f $select) {
	die "\n\nERROR: Cannot find $select. Check file path.\n\n";
}

my $outdir = "Plink_Fst_results"; #create directory folder for results
#my $outdir = "Plink_Fst_results_Prelim"; #create directory folder for results

system "mkdir $outdir";



open SELECT, "$select";

my @pairs = <SELECT>; # all pairs of pops to calculate Fst for

close SELECT;

my %added_pop; #popN => pop1+pop2

foreach my $p (@pairs) {

	my %select; #  population => 0   ....just need key to exist

	
	 chomp $p;
	 
	 
	 
	 my @p = split /\s/, $p;
	 
	 foreach (@p) {
	 
	 	if ($_ =~ /\+/) {
	 		my @l = split /\+/, $_;
	 		foreach my $i (@l) {
	 			unless (exists $select{$i}) {
	 				$select{$i} = 0;
	 			}
	 			unless (exists $added_pop{$i}) {
	 				$added_pop{$i} = $_;
	 			}
	 		}
	 		
	 	} else {
	 
	 		unless (exists $select{$_}) {
	 			$select{$_} = 0;
	 		}
	 	}
	 }

	my $plink_out = join "_", @p;


	open POP, "$pop";

	my %id; # ID => population 

	while (<POP>) {
		my $line = $_;
		chomp $line;
		my @l = split /\s/, $line;
		my $id = $l[0];
		my $p = $l[3];
		if (exists $select{$p}) {
		
			if (exists $added_pop{$p}) {
				unless (exists $id{$id}) {
					$id{$id} = $added_pop{$p};
				}
			} else {
		
				unless (exists $id{$id}) {
					$id{$id} = $p;
				}
			
			}
		}
	}

	close POP;


	open FAM, "$fam";

	#my @file = <FAM>;

	my %fid; # IID => FID

	while (<FAM>) {
		my $line = $_;
		chomp $line;
		my @l = split /\s/, $line;
		my $fid = $l[0];
		my $iid = $l[1];
		if (exists $id{$iid}) {
			unless (exists $fid{$iid}) {
				$fid{$iid} = $fid;
			}
		 }
	}

	close FAM;


	my %keep; # ID => 0
	
	open ID, "$IDselect";

	my @ID = <ID>;

	
	
	foreach my $i (@ID) {
		chomp $i;
		unless (exists $keep{$i}) {
			$keep{$i} = 0;
		}
	}

	close ID;


	my $within = $plink_out."_within.txt";

	open OUT, ">$within";

	foreach my $i (keys %fid) {
	
		if (exists $keep{$i}) {
			print OUT "$fid{$i}\t$i\t$id{$i}\n";
		}
	}


	#Subset file to only include individuals in source populations that have low admixture
	#using IDs in txt file - IDselect file includes all test individuals and selected source indivdual IDs

	
	close OUT;


	#Run Fst analysis:

	system "plink --bfile $bfile --within $within --fst --out $outdir/$plink_out";


 	system "mv $within $outdir";

}

exit;



