#!/usr/bin/perl

use strict;
use warnings;

####################################################
#                 pairwise.pl                      #
# Creates txt file with pairs of source/test pops  #
####################################################

#only includes pairs that have at least one source population
#output txt file to be used for fst 

my $input = $ARGV[0]; #txt file with list of all test and source populations #TestSourcePops.txt
my $source_list = $ARGV[1]; #txt file with list of just source populations #SourcePops.txt
my $out = $ARGV[2]; #output txt file with pairs of each source and test pops 
#my $strings = [qw(Anuak Yoruba Amhara Druze)];

open INPUT, $input;
#open SOURCE, $source_list;
open SOURCE, "$source_list";
open OUT, ">$out";


my @list = <INPUT>;
foreach (@list) {
	chomp $_;
	$_ =~ s/\s/\+/g;
}
my $strings = [@list];

sub combine;



my @full_list;

push @full_list, "@$_" for combine $strings, 2;

#print OUT "@$_\n" for combine $strings, 2;
#close OUT;

#only keep pairs that include at least one source population:

my @source = <SOURCE>;
foreach(@source){
	chomp $_;
	$_ =~ s/\s/\+/g;
}


my %source;  # source => 0   ...just want key to exist 

foreach (@source) {
	if ($_ =~ /\+/) {
		my @l = split /\+/, $_;
		unless (exists $source{"$l[0]\+$l[1]"}) {
			$source{"$l[0]\+$l[1]"} = 0;
		}
		unless (exists $source{"$l[1]\+$l[0]"}) {
			$source{"$l[1]\+$l[0]"} = 0;
		}
	}
	else {
		unless (exists $source{$_}) {
			$source{$_} = 0;
		}
	}
}

#my @full_list = combine $strings, 2;
my @keep = <SOURCE>; #combinations to keep
my $source = [@keep];


foreach (@full_list) {
	my @l = split /\s/, $_;
	my $p1 = $l[0]; 
	my $p2 = $l[1];
	
	if (exists $source{$p1} or exists $source{$p2}) {
		push @keep, $_;
	}
	
}

foreach (@keep) {
	print OUT "$_\n";
}

close OUT;
close SOURCE;
close INPUT;


####

sub combine {

  my ($list, $n) = @_;
  die "Insufficient list members" if $n > @$list;

  return map [$_], @$list if $n <= 1;

  my @comb;

  for (my $i = 0; $i+$n <= @$list; ++$i) {
    my $val  = $list->[$i];
    my @rest = @$list[$i+1..$#$list];
    push @comb, [$val, @$_] for combine \@rest, $n-1;
  }
  
  return @comb;

}


exit;


