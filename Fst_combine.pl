#!/usr/bin/perl

use strict;
use warnings;


my $indir = $ARGV[0]; # directory of Plink Fst results
my $location = $ARGV[1]; # population location
my $out_table = $ARGV[2]; #R table result


unless ($indir and $location and $out_table) {
	die "\n\nERROR: Not enough arguments.\n\n";
} 

unless (-f $location) {
	die "\n\nERROR: cannot find $location. Check file path.\n\n";
}

unless (-d $indir) {
	die "\n\nERROR: Cannot find Fst Results director $indir. Check file path.\n\n";
}


my %pop_loc; # population => region


open LOC, "$location";

<LOC>;

while (<LOC>) {
	my $line = $_;
	chomp $line;
	my @l = split ",", $line;
	my $p = $l[0];
	my $c = $l[1];
	unless (exists $pop_loc{$p}) {
		$pop_loc{$p} = $c;
	}
}

close LOC;




my @files = `ls $indir`;

my @fst_files; # just the .fst files


my $source_fst_file;


foreach (@files) {
	chomp $_;
	if ($_ =~ /\.fst/) {
		
		if ($_ =~ /Yoruba_YRI\+Anuak/ and $_ =~ /Druze\+iran/) {
			$source_fst_file = $_;
		} else {
			push @fst_files, $_;
			print "$_\n";
		}
		
	}
}


unless ($source_fst_file) {
	die "\n\nERROR: Cannot find source Fst file.\n\n";
}


my %Fst_source; # RS => FST

open SOURCE, "$indir/$source_fst_file";

<SOURCE>;

while (<SOURCE>) {
	my $line = $_;
	chomp $line;
	my @l = split /\s/, $line;
	
	my $rs = $l[1];
	my $fst = $l[4];
	
	unless (exists $Fst_source{$rs}) {
		$Fst_source{$rs} = $fst;
	}
	
}

close SOURCE;



###    CHR	SNP	POS	NMISS	FST







my %test_files; # testpop => Fst_file1 Fst_file_2

foreach my $f (@fst_files) {
	
	my $file = $f;
	
	$f =~ s/\.fst//;
	$f =~ s/Yoruba_YRI\+Anuak//;
	$f =~ s/Druze\+iran//;
	$f =~ s/_$//;
	$f =~ s/^_//;
	
	if (exists $test_files{$f}) {
		$test_files{$f} = $test_files{$f}." $file";
	} else {
		$test_files{$f} = $file;
	}
	
}


open OUT, ">$out_table";

print OUT "RS\tPOP\tREG\tCHR\tPOS\tFST_ME\tFST_AFR\tFST_SOURCE\n";


#foreach my $i (keys %test_files) {
#	print "$i\t$test_files{$i}\n";
#}


foreach my $i (keys %test_files) {

	my @files = split /\s/, $test_files{$i};
	
	
	my $af;
	my $me;
	
	
	if ($files[0] =~ /Yoruba_YRI/) {
		$af = $files[0];
	} elsif ($files[0] =~ /Druze/) {
		$me = $files[0];
	}
	
	if ($files[1] =~ /Yoruba_YRI/) {
		$af = $files[1];
	} elsif ($files[1] =~ /Druze/) {
		$me = $files[1];
	}
	
	print "ME file: $me\nAF file: $af\n";
	
	
	my %Af_fst; #RS => fst
	
	open AF, "$indir/$af";
	<AF>;
	
	while (<AF>) {
		my $line = $_;
		chomp $line;
		my @l = split /\s/, $line;
		my $rs = $l[1];
		my $fst = $l[4];
		
		unless (exists $Af_fst{$rs}) {
			$Af_fst{$rs} = $fst;
		}
		
	}
	
	close AF;
	
	open ME, "$indir/$me";
	
	<ME>;
	
	while (<ME>) {
		my $line = $_;
		chomp $line;
		my @l = split /\s/, $line;
		
		my $chr = $l[0];
		my $rs = $l[1];
		my $pos = $l[2];
		my $fst_me = $l[4];

		my $reg;

		if (exists $pop_loc{$i}) {
			$reg = $pop_loc{$i}; 
		} else {
			$reg = "NA";
		}

		my $fst_src;

		if (exists $Fst_source{$rs}) {
			$fst_src = $Fst_source{$rs};
		} else {
			$fst_src = "NA";
		}
		
		my $fst_af;
		
		if (exists $Af_fst{$rs}) {
			$fst_af = $Af_fst{$rs};
		} else {
			$fst_af = "NA";
		}
		
		
		
		print OUT "$rs\t$i\t$reg\t$chr\t$pos\t$fst_me\t$fst_af\t$fst_src\n";
	
	}
	

	close ME;
	

}

close OUT;



#foreach my $f (@fst_files) {
#
#	my $filename = $f;
#	
#	$f =~ s/\.fst//;
#	
#	my @source;
#	
#	while ($f =~ /[Yoruba_YRI\+Anuak|Druze\+iran]/g) {
#		push @source, $&;
#	}
#	$f =~ s/Yoruba_YRI//;
#	$f =~ s/Druze\+iran//;
#	
#	my $test;
#	
#	if (scalar @source == 1) {
#		$f =~ s/_$//;
#		$test = $f;
#	}
#	
#	open FST, "$indir/$filname";
#	
#	<FST>;
#	
#	while (<FST>) {
#		my $line = $_;
#		chomp $line;
#		my @l = split /\s/, $line;
#		
#		my $chr = $l[0];
#		my $rs = $l[1];
#		my $pos = $l[2];
#		my $n = $l[3];
#		my $fst = $l[4];
#		
#		my $reg = $pop_loc[$test];
#		
#		my $fst_source = $Fst_source[$rs];
#		
#		
#		
#	}
#
#	
#	
#
#
#}












exit;